package com.devcamp.S10.Task5630;

public class Employee {
    private int id = 0;
    private String firstName = "none";
    private String lastName = "none";
    private int salary = 0;

    public Employee() {
        super();
    }

    public Employee(int id, String firstName, String lastName, int salary) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.salary = salary;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public int getAnnualSalary() {
        return salary * 12;
    }

    public int raiseSalary(float percent) {
        float newSalary = (float) salary * (1 + (percent / 100));
        return (int) newSalary;
    }

    @Override
    public String toString() {
        return String.format("Employee[id= %s ,name= %s %s ,salary= %s]", id, firstName, lastName, salary);
    }
}

import com.devcamp.S10.Task5630.Employee;

public class App {
    public static void main(String[] args) throws Exception {
        Employee employee1 = new Employee();
        Employee employee2 = new Employee(8, "Hao", "Le", 50000000);
        System.out.println(employee1.toString());
        System.out.println(employee2.toString());
        int annualSalary1 = employee1.getAnnualSalary();
        int annualSalary2 = employee2.getAnnualSalary();
        System.out.println("Employee 1: Annual Salary= " + annualSalary1);
        System.out.println("Employee 2: Annual Salary= " + annualSalary2);
        int raiseSalary1 = employee1.raiseSalary(50);
        int raiseSalary2 = employee2.raiseSalary(15);
        System.out.println("Employee 1:After raise Salary= " + raiseSalary1);
        System.out.println("Employee 2:After raise Salary= " + raiseSalary2);
    }
}
